class AddAvatarToArticles < ActiveRecord::Migration[5.1]
  def change
    add_attachment :articles, :avatar
  end
end
