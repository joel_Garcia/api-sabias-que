class Article < ApplicationRecord
  
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  validates :title, presence: true
  validates :description, presence: true
  validates :avatar, presence: true
  validates :category, presence: true

  def as_json options={}
    {
      id: id,
      title: title,
      description: description,
      avatar_url: avatar.url,
      category: category
    }
  end
end
