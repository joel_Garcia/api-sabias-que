class Api::V1::ArticlesController < ApplicationController
  #def index
  #  articles = Article.where(category: "Science")
  #  render json: articles, status: :ok
  #end
  #def show
  #	article = Article.find(params[:id])
  #	render json: article, status: :ok
  #end
  def biology
    biology = Article.where(category: "Biology")
    render json: biology, status: :ok
  end
  def biology_show
  	biology = Article.find(params[:id])
  	render json: biology, status: :ok
  end
  def astronomy
    astronomy = Article.where(category: "Astronomy")
    render json: astronomy, status: :ok
  end
  def astronomy_show
  	astronomy = Article.find(params[:id])
  	render json: astronomy, status: :ok
  end
  def technology
    technology = Article.where(category: "Technology")
    render json: technology, status: :ok
  end
  def technology_show
  	technology = Article.find(params[:id])
  	render json: technology, status: :ok
  end
  def history
    history = Article.where(category: "History")
    render json: history, status: :ok
  end
  def entertainment
    entertainment = Article.where(category: "Entertainment")
    render json: entertainment, status: :ok
  end
  def culture
    culture = Article.where(category: "Culture")
    render json: culture, status: :ok
  end
  def search
    search = Article.where("title ILIKE ?", "%#{params[:search]}%").or(Article.where("description ILIKE ?", "%#{params[:search]}%"))
    render json: search, status: :ok
  end
end
