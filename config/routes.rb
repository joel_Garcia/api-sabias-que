Rails.application.routes.draw do
  resources :articles
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  namespace :api, defaults: {format: :json} do
   namespace :v1 do
   	#get '/articles/science' => 'articles#index'
    #get '/articles/science/:id' => 'articles#show'
    get '/articles/biology' => 'articles#biology'
    get '/articles/biology/:id' => 'articles#biology_show'
    get '/articles/astronomy' => 'articles#astronomy'
    get '/articles/astronomy/:id' => 'articles#astronomy_show'
    get '/articles/technology' => 'articles#technology'
    get '/articles/technology/:id' => 'articles#technology'
    get '/articles/history' => 'articles#history'
    get '/articles/entertainment' => 'articles#entertainment'
    get '/articles/culture' => 'articles#culture'
    get '/articles/search/:search' => 'articles#search'
   end
  end
end
